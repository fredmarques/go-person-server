package main

import (
	"go-person-server/server"
	"log"
)

func main() {
	log.Println("Starting server")
	server.StartServer()
}
