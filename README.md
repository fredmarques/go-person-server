# Person Server

This is a simple golang rest server that manages a Person entity

## Project files

```
├── config              -----> stores project's config variables 
│   └── config.go       --> project config variables
├── go.mod
├── go.sum
├── main.go             --> project's entry point
├── models              -----> stores project's models like Person entity
│   └── person.go       --> Person entity
├── server              -----> package to handle server routing and controllers.
│   ├── middlewares.go  --> auth and headers middlewares
│   ├── person.go       --> Person controller
│   ├── routes.go       --> routing
│   └── serverErrors.go --> default errors
└── store               -----> handles in memory person storage
    ├── README.md
    ├── person.go       --> Person store implementation
    ├── person_test.go  --> unit test person store
    └── storeErrors.go  --> default erros for store operations
```

At *server/routes.go line 16* **Auth middleware** is commented out due it's empty logic.
## Running project

`go run main.go`

## Testing project

### Unit testing
`go test`


## Rest API

Assuming server is running at **localhost** using default port **:8081**

* Get Person by Age: 
```
curl --request GET --url http://localhost:8081/person/USER_NAME
```

* Query Person by Age range: 
```
curl --request GET --url http://localhost:8081/person/age?min=24&max=42
```

* Add a new Person
```
curl --request POST \
   --url http://localhost:8081/person \
   --header 'content-type: application/json' \
   --data '{
 	"name": "",
 	"lastName": "",
 	"age": 0,
 	"address": ""
 }'
 ```
 
 * Update Person by Name
 ```
 curl --request PUT \
   --url http://localhost:8081/person \
   --header 'content-type: application/json' \
   --data '{
 	"name": "",
 	"lastName": "",
 	"age": 0,
 	"address": ""
 }'
 ```
 
 * Remove Person by Name
 ```
curl --request DELETE \
  --url http://localhost:8081/person/USER_NAME
 ```
 
 
 -------
 Some project decisions:
 
 1. Indexing Person entity by name for simplicity. In a real system, a int or larger number type might be used as identity
 
 2. Creating indexes to name and age to simulate a real RDBMS system. This is a simple and poor implementation, just for fun.
 -------
