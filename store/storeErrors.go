package store

import "errors"

var (
	PersonAlreadyExists = errors.New("person already exists")
	PersonNotFound      = errors.New("person not found")
)
