package store

import (
	"testing"
	"go-person-server/models"
)

func afterEach() {
	personNameIndex = make(map[string]*models.Person)
	personAgeIndex = make(map[int][]*models.Person)
}

var personStore = &PersonStore{}

func TestAddPerson(t *testing.T) {
	person := models.Person{Name: "Frederico", LastName: "Marques", Age: 26, Address: "Earth"}

	if err := personStore.AddPerson(person); err != nil {
		t.Fail()
	}

	if inserted, ok := personNameIndex[person.Name]; !ok || *inserted != person {
		t.Fail()
	}
	afterEach()
}

func TestGetPersonByName(t *testing.T) {
	person := models.Person{Name: "Frederico", LastName: "Marques", Age: 26, Address: "Earth"}

	personNameIndex[person.Name] = &person

	retrievedPerson, err := personStore.GetPersonByName(person.Name)

	if err != nil || retrievedPerson != person {
		t.Fail()
	}
	afterEach()
}

func TestUpdatePerson(t *testing.T) {
	person := models.Person{Name: "Frederico", LastName: "Marques", Age: 26, Address: "Earth"}
	_ = personStore.AddPerson(person)

	nextPerson := models.Person{Name: "Frederico", LastName: "Test", Age: 26, Address: "Earth"}
	err := personStore.UpdatePerson(nextPerson)

	if err != nil {
		t.Fail()
	}
	afterEach()
}

func TestDeletePerson(t *testing.T) {
	person := models.Person{Name: "Frederico", LastName: "Marques", Age: 26, Address: "Earth"}
	_ = personStore.AddPerson(person)

	personStore.DeletePerson(person.Name)

	_, err := personStore.GetPersonByName(person.Name)

	if err == nil {
		t.Fail()
	}
	afterEach()
}

func TestGetPersonAtAge(t *testing.T) {
	testPersons := models.PersonArray{
		models.Person{Name: "P1", LastName: "LN1", Age: 26, Address: "Earth"},
		models.Person{Name: "P2", LastName: "LN2", Age: 26, Address: "Earth"},
		models.Person{Name: "P3", LastName: "LN3", Age: 26, Address: "Earth"},
	}

	for _, person := range testPersons {
		err := personStore.AddPerson(person)
		if err != nil {
			t.Fail()
		}
	}

	persons := personStore.GetPersonAtAge(26)

	if len(persons) != len(testPersons) {
		t.Fail()
	}

	afterEach()
}
