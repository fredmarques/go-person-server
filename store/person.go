package store

import (
	"go-person-server/models"
)

type IPersonStore interface {
	AddPerson(person models.Person) error
	GetPersonByName(name string) (models.Person, error)
	GetPersonAtAge(age int) models.PersonArray
	UpdatePerson(person models.Person) error
	DeletePerson(name string)
}

type PersonStore struct{}

var personNameIndex = make(map[string]*models.Person)
var personAgeIndex = make(map[int][]*models.Person)

func (*PersonStore) AddPerson(person models.Person) error {
	_, ok := personNameIndex[person.Name]

	if ok {
		return PersonAlreadyExists
	}

	personNameIndex[person.Name] = &person
	addItemIntoAgeIndex(person)

	return nil
}

func (*PersonStore) GetPersonByName(name string) (models.Person, error) {
	if person, ok := personNameIndex[name]; ok {
		return *person, nil
	}

	return models.Person{}, PersonNotFound
}

func (*PersonStore) GetPersonAtAge(age int) models.PersonArray {
	if personList, ok := personAgeIndex[age]; ok {
		persons := make(models.PersonArray, len(personList))
		for idx, value := range personList {
			persons[idx] = *value
		}
		return persons
	}

	return models.PersonArray{}
}

func (*PersonStore) UpdatePerson(person models.Person) error {
	prevPerson, ok := personNameIndex[person.Name]

	if !ok {
		return PersonNotFound
	}

	updateIndexes(*prevPerson, person)

	return nil
}

func (p *PersonStore) DeletePerson(name string) {
	if person, err := p.GetPersonByName(name); err == nil {
		delete(personNameIndex, person.Name)
		removeItemFromAgeIndex(person)
	}
}

func updateIndexes(prevPerson models.Person, nextPerson models.Person) {
	delete(personNameIndex, prevPerson.Name)
	removeItemFromAgeIndex(prevPerson)

	personNameIndex[nextPerson.Name] = &nextPerson
	addItemIntoAgeIndex(nextPerson)
}

func addItemIntoAgeIndex(person models.Person) {
	if val, ok := personAgeIndex[person.Age]; ok {
		personAgeIndex[person.Age] = append(val, &person)
	} else {
		personAgeIndex[person.Age] = append(make([]*models.Person, 0), &person)
	}
}

func removeItemFromAgeIndex(person models.Person) {
	ageList := personAgeIndex[person.Age]
	for key, value := range ageList {
		if value.Name == person.Name {
			// rewrites item to be remove with list last item
			ageList[key] = ageList[len(ageList)-1]
			// exclude last item (write zero/nil value)
			ageList[len(ageList)-1] = nil
			ageList = ageList[:len(ageList)-1]
			break
		}
	}
}
