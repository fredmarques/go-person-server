package models

// Person entity
type Person struct {
	Name     string `json:"name"`
	LastName string `json:"lastName"`
	Age      int    `json:"age"`
	Address  string `json:"address"`
}

// PersonArray is an array of articles
type PersonArray []Person
