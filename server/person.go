package server

import (
	"encoding/json"
	"net/http"
	"strconv"
	"go-person-server/models"
	"go-person-server/store"

	"github.com/gorilla/mux"
)

type IPersonHandler interface {
	GetPersonByName(w http.ResponseWriter, r *http.Request)
	AddPerson(w http.ResponseWriter, r *http.Request)
	UpdatePerson(w http.ResponseWriter, r *http.Request)
	RemovePerson(w http.ResponseWriter, r *http.Request)
	QueryPersonAtAge(w http.ResponseWriter, r *http.Request)
}

type PersonHandler struct{}

var personStore store.IPersonStore

func init() {
	personStore = &store.PersonStore{}
}

func (*PersonHandler) GetPersonByName(w http.ResponseWriter, r *http.Request) {
	if name, ok := mux.Vars(r)["name"]; ok {
		person, err := personStore.GetPersonByName(name)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write([]byte(err.Error()))
		} else {
			_ = json.NewEncoder(w).Encode(person)
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}

func (*PersonHandler) AddPerson(w http.ResponseWriter, r *http.Request) {
	var p models.Person
	err := json.NewDecoder(r.Body).Decode(&p)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = personStore.AddPerson(p)
	if err != nil {
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (*PersonHandler) UpdatePerson(w http.ResponseWriter, r *http.Request) {
	var p models.Person
	err := json.NewDecoder(r.Body).Decode(&p)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(MalformedBodyData.Error()))
		return
	}

	err = personStore.UpdatePerson(p)

	if err != nil {
		_, _ = w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (*PersonHandler) RemovePerson(w http.ResponseWriter, r *http.Request) {
	if name, ok := mux.Vars(r)["name"]; ok {
		personStore.DeletePerson(name)
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(MissingNameParameter.Error()))
	}
}

func (*PersonHandler) QueryPersonAtAge(w http.ResponseWriter, r *http.Request) {
	min, minErr := strconv.Atoi(r.URL.Query().Get("min"))
	max, maxErr := strconv.Atoi(r.URL.Query().Get("max"))

	if minErr != nil || maxErr != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var personList models.PersonArray

	for i:= min; i<=max; i++  {
		pList := personStore.GetPersonAtAge(i)
		personList = append(personList, pList...)
	}
	_ = json.NewEncoder(w).Encode(personList)
}
