package server

import "errors"

var (
	UnexpectedSigningMethod = errors.New("unexpected signing method")
	MalformedBodyData       = errors.New("malformed body data")
	MissingNameParameter    = errors.New("missing name<string> parameter")
)
