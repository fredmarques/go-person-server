package server

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var router = mux.NewRouter().StrictSlash(true)

var personHandler IPersonHandler

func init() {
	//router.Use(Auth)
	router.Use(SetJSONType)

	personHandler = &PersonHandler{}

	router.Path("/person/name/{name}").HandlerFunc(personHandler.GetPersonByName).Methods("GET")
	router.Path("/person/age").HandlerFunc(personHandler.QueryPersonAtAge).Methods("GET").Queries("min", "{min}", "max", "{max}")
	router.Path("/person").HandlerFunc(personHandler.AddPerson).Methods("POST")
	router.Path("/person").HandlerFunc(personHandler.UpdatePerson).Methods("PUT")
	router.Path("/person/{name}").HandlerFunc(personHandler.RemovePerson).Methods("DELETE")
}

func StartServer() {
	log.Fatal(http.ListenAndServe(":8081", router))
}
